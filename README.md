INTRODUCTION
------------

This module provides an option to configure Facebook Pixel and Twitter
conversion tracking in your site. The module is designed in such a way that
 it can be extendable to give support for any other social tracking in future.

For the initial launch, the module gives support for the PageView (all pages)
event, in further releases, module will provide options for different events.

REQUIREMENTS
------------

Module doesn't have any special requirements.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.

CONFIGURATION
-------------

* Social Tracking provide config entity which have in build plugins for 
 Facebook and Twitter.
* Go to ```admin/config/services/social_tracking``` and configure the
 tracking with specific pixel Id.
* Module provide ```administer social tracking``` permission to give access
 to config pages.
* Also it provides role based filtering on each Tracking entity.

MAINTAINERS
-----------

Current maintainers:
 * Milind Kagdelwar (milind0308) -
https://www.drupal.org/u/milind0308
