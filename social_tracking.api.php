<?php

/**
 * @file
 * Hooks specific to the Social Tracking module.
 */

use Drupal\social_tracking\Entity\SocialTrackingEntityInterface;

/**
 * Alter the script code for pages.
 *
 * @param string $script_code
 *   The script code.
 * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $tracking_entity
 *   Tracking entity to check.
 */
function hook_social_tracking_script_code_alter(&$script_code, SocialTrackingEntityInterface $tracking_entity) {
  if ($tracking_entity->id() == 'id') {
    $script_code = 'Altered script code based on entity';
  }

  if ($tracking_entity->getTrackingProvider() == 'facebook') {
    $script_code = 'Altered facebook script code';
  }

  if ($tracking_entity->getTrackingProvider() == 'twitter') {
    $script_code = 'Altered twitter script code';
  }
}

/**
 * Alter the noscript code for pages.
 *
 * @param string $noscript_code
 *   The noscript code.
 * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $tracking_entity
 *   Tracking entity to check.
 */
function hook_social_tracking_no_script_code_alter(&$noscript_code, SocialTrackingEntityInterface $tracking_entity) {
  if ($tracking_entity->id() == 'id') {
    $noscript_code = 'Altered no script code based on entity';
  }

  if ($tracking_entity->getTrackingProvider() == 'facebook') {
    $noscript_code = 'Altered facebook no script code';
  }

  if ($tracking_entity->getTrackingProvider() == 'twitter') {
    $noscript_code = 'Altered twitter no script code';
  }
}

/**
 * Modify the list of available Social Tracking plugins.
 *
 * This hook may be used to modify plugin properties after they have been
 * specified by other modules.
 *
 * @param array $plugins
 *   An array of all the existing plugin definitions, passed by reference.
 *
 * @see \Drupal\social_tracking\Plugin\SocialTrackingPluginManager
 */
function hook_social_tracking_plugin_info_alter(array &$plugins) {
  $plugins['plugin']['label'] = t('Different name');
}
