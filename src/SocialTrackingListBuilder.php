<?php

namespace Drupal\social_tracking;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of Social Tracking.
 *
 * @see \Drupal\social_tracking\Entity\SocialTracking
 */
class SocialTrackingListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Tracking ID');
    $header['provider'] = [
      'data' => $this->t('Provider'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['provider'] = $entity->get('tracking_provider');
    $row['status'] = $entity->isEnabled() ? 'active' : 'inactive';
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);
    uasort($operations, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('No social Tracking are available. <a href=":link">Add Social Tracking</a>.', [':link' => Url::fromRoute('entity.social_tracking.add_form')->toString()]);
    return $build;
  }

}
