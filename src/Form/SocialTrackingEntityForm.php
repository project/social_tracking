<?php

namespace Drupal\social_tracking\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\social_tracking\SocialTrackingBuilderServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TrackingEntityForm.
 *
 * @package Drupal\social_tracking\Form
 */
class SocialTrackingEntityForm extends EntityForm {

  /**
   * The tracking builder service.
   *
   * @var \Drupal\social_tracking\SocialTrackingBuilderServiceInterface
   */
  protected $trackingBuilder;

  /**
   * TrackingEntityForm constructor.
   *
   * @param \Drupal\social_tracking\SocialTrackingBuilderServiceInterface $tracking_builder
   *   The module handler.
   */
  public function __construct(SocialTrackingBuilderServiceInterface $tracking_builder) {
    $this->trackingBuilder = $tracking_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('social_tracking.tracking_builder'));
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $config */
    $config = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config->label(),
      '#description' => $this->t("Label for the Configuration Social Tracking Setting."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $config->id(),
      '#machine_name' => [
        'exists' => '\Drupal\social_tracking\Entity\SocialTracking::load',
      ],
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Tracking'),
      '#default_value' => $config->get('status'),
    ];

    $form['tracking_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Tracking Provider'),
      '#options' => $this->trackingBuilder->getSocialTrackingPlugins(),
      '#required' => TRUE,
      '#default_value' => $config->get('tracking_provider') ?? NULL,
    ];

    $form['basic_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Basic settings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['basic_settings']['page_view_notice'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('PageView event is by default enabled on all pages.') . '</p>',
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['basic_settings']['tracking_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tracking ID'),
      '#default_value' => $config->get('tracking_id'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['basic_settings']['exclude_admin_pages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude admin pages'),
      '#default_value' => $config->get('exclude_admin_pages'),
    ];

    $form['basic_settings']['excluded_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Exclude Tracking for the following roles'),
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#default_value' => $config->get('excluded_roles') ?? [],
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $tracking = $this->entity;
    $status = $tracking->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Tracking.', [
          '%label' => $tracking->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Tracking.', [
          '%label' => $tracking->label(),
        ]));
    }
    $form_state->setRedirectUrl($tracking->toUrl('collection'));
  }

  /**
   * Gets content entities for which it is possible to enable ViewContent event.
   *
   * @return array
   *   The list of entities.
   */
  protected function getViewContentEntities() {
    $entities = [];

    if ($this->entityTypeManager->hasDefinition('node_type')) {
      $content_types = $this->entityTypeManager
        ->getStorage('node_type')
        ->loadMultiple();

      foreach ($content_types as $content_type) {
        $entities['node:' . $content_type->getOriginalId()] = $this->t('Node: @label', ['@label' => $content_type->label()]);
      }
    }

    if ($this->entityTypeManager->hasDefinition('taxonomy_vocabulary')) {
      $vocabularies = $this->entityTypeManager
        ->getStorage('taxonomy_vocabulary')
        ->loadMultiple();

      foreach ($vocabularies as $vocabulary) {
        $entities['taxonomy_term:' . $vocabulary->getOriginalId()] = $this->t('Taxonomy: @label', ['@label' => $vocabulary->label()]);
      }
    }

    return $entities;
  }

}
