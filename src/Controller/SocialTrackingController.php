<?php

namespace Drupal\social_tracking\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\social_tracking\Entity\SocialTrackingEntityInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class SocialTrackingController.
 *
 * @package Drupal\social_tracking\Controller
 */
class SocialTrackingController extends ControllerBase {

  /**
   * Enables a Social Tracking object.
   *
   * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $social_tracking
   *   The Social Tracking object to enable.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the Social Tracking listing page.
   */
  public function enable(SocialTrackingEntityInterface $social_tracking) {
    $social_tracking->enable()->save();
    $this->messenger()->addStatus($this->t('Settings have been updated.'));
    return $this->redirect('entity.social_tracking.collection');
  }

  /**
   * Disables a Social Tracking object.
   *
   * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $social_tracking
   *   The Social Tracking object to disable.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the Social Tracking listing page.
   */
  public function disable(SocialTrackingEntityInterface $social_tracking) {
    $social_tracking->disable()->save();
    $this->messenger()->addStatus($this->t('Settings have been updated.'));
    return $this->redirect('entity.social_tracking.collection');
  }

  /**
   * Returns a redirect response object for the specified route.
   *
   * @param string $route_name
   *   The name of the route to which to redirect.
   * @param array $route_parameters
   *   (optional) Parameters for the route.
   * @param array $options
   *   (optional) An associative array of additional options.
   * @param int $status
   *   (optional) The HTTP redirect status code for the redirect. The default is
   *   302 Found.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  protected function redirect($route_name, array $route_parameters = [], array $options = [], $status = 302) {
    $options['absolute'] = TRUE;
    return new RedirectResponse(Url::fromRoute($route_name, $route_parameters, $options)->toString(), $status);
  }

}
