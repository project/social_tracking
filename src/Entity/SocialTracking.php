<?php

namespace Drupal\social_tracking\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Component\Utility\Html;

/**
 * Defines the Social Tracking entity.
 *
 * @ConfigEntityType(
 *   id = "social_tracking",
 *   label = @Translation("Social Tracking"),
 *   module = "social_tracking",
 *   handlers = {
 *     "list_builder" = "Drupal\social_tracking\SocialTrackingListBuilder",
 *     "form" = {
 *       "add" = "Drupal\social_tracking\Form\SocialTrackingEntityForm",
 *       "edit" = "Drupal\social_tracking\Form\SocialTrackingEntityForm",
 *       "delete" = "Drupal\social_tracking\Form\SocialTrackingEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "tracking",
 *   admin_permission = "administer social tracking",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/services/social_tracking/add",
 *     "edit-form" = "/admin/config/services/social_tracking/manage/{social_tracking}",
 *     "delete-form" = "/admin/config/services/social_tracking/manage/{social_tracking}/delete",
 *     "enable" = "/admin/config/services/social_tracking/{social_tracking}/enable",
 *     "disable" = "/admin/config/services/social_tracking/{social_tracking}/disable",
 *     "collection" = "/admin/config/services/social_tracking"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "tracking_id",
 *     "tracking_provider",
 *     "exclude_admin_pages",
 *     "excluded_roles"
 *   }
 * )
 */
class SocialTracking extends ConfigEntityBase implements SocialTrackingEntityInterface {

  /**
   * The Social Tracking ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Social Tracking label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Social Tracking ID.
   *
   * @var string
   */
  protected $tracking_id = NULL;

  /**
   * The Social Tracking Plugin Provider.
   *
   * @var string
   */
  protected $tracking_provider = NULL;

  /**
   * The Social Tracking Exclude admin pages.
   *
   * @var string
   */
  protected $exclude_admin_pages = FALSE;

  /**
   * The Social Tracking Excluded roles.
   *
   * @var array
   */
  protected $excluded_roles = [];

  /**
   * {@inheritdoc}
   */
  public function getTrackingId() {
    return Html::escape($this->get('tracking_id'));
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->get('status');
  }

  /**
   * {@inheritdoc}
   */
  public function isExcludeAdminPages() {
    return (bool) $this->get('exclude_admin_pages');
  }

  /**
   * {@inheritdoc}
   */
  public function getExcludedRoles() {
    return $this->get('excluded_roles');
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingProvider() {
    return $this->get('tracking_provider');
  }

}
