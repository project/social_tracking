<?php

namespace Drupal\social_tracking\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Configuration Social Tracking entities.
 */
interface SocialTrackingEntityInterface extends ConfigEntityInterface {

  /**
   * Get Tracking ID.
   *
   * @return mixed|null
   *   Return tracking ID.
   */
  public function getTrackingId();

  /**
   * Get Tracking enabled.
   *
   * @return mixed|null
   *   Return tracking Enabled.
   */
  public function isEnabled();

  /**
   * Get isExcludeAdminPages.
   *
   * @return mixed|null
   *   Return is AdminPages exclude.
   */
  public function isExcludeAdminPages();

  /**
   * Get getExcludedRoles.
   *
   * @return mixed|null
   *   Return roles.
   */
  public function getExcludedRoles();

  /**
   * Get Tracking provider.
   *
   * @return mixed|null
   *   Return tracking provider.
   */
  public function getTrackingProvider();

}
