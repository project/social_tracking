<?php

namespace Drupal\social_tracking\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a social_tracking type annotation object.
 *
 * @Annotation
 */
class SocialTracking extends Plugin {

  /**
   * The plugin ID of the Tracking type.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the Tracking type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the Tracking type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The script position of the Tracking type.
   *
   * @var string
   */
  public $scriptLocation;

}
