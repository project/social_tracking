<?php

namespace Drupal\social_tracking;

use Drupal\social_tracking\Entity\SocialTrackingEntityInterface;

/**
 * Interface TrackingBuilderServiceInterface.
 */
interface SocialTrackingBuilderServiceInterface {

  /**
   * Gets script code.
   *
   * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $tracking
   *   Tracking entity.
   *
   * @return mixed
   *   Return Tracking script code.
   */
  public function getTrackingScriptCode(SocialTrackingEntityInterface $tracking);

  /**
   * Gets no-script code.
   *
   * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $tracking
   *   Tracking entity.
   *
   * @return string
   *   Return Tracking no-script code.
   */
  public function getTrackingNoScriptCode(SocialTrackingEntityInterface $tracking);

  /**
   * Checks tracking should be enabled.
   *
   * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $tracking
   *   Tracking entity.
   *
   * @return bool
   *   True if enabled, False otherwise.
   */
  public function isEnabled(SocialTrackingEntityInterface $tracking);

  /**
   * Get Tracking Entities.
   *
   * @param array|null $tracking_ids
   *   Any specific id to return.
   *
   * @return mixed
   *   Return entities associated.
   */
  public function getSocialTrackingEntities(array $tracking_ids = NULL);

  /**
   * Returns a Plugin Definition for tracking provider.
   *
   * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $tracking
   *   Tracking provider for which plugin definition is required.
   *
   * @return mixed
   *   The plugin definition.
   */
  public function getSocialTrackingProviderPlugin(SocialTrackingEntityInterface $tracking);

  /**
   * Returns a list of plugins, for use in forms.
   *
   * @return array
   *   The list of plugins, indexed by ID.
   */
  public function getSocialTrackingPlugins();

  /**
   * Returns tracking plugin script location.
   *
   * @param \Drupal\social_tracking\Entity\SocialTrackingEntityInterface $tracking
   *   Tracking provider for which plugin definition is required.
   *
   * @return mixed
   *   The tracking plugin script location.
   */
  public function getTrackingScriptLocation(SocialTrackingEntityInterface $tracking);

}
