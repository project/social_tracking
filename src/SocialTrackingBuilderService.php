<?php

namespace Drupal\social_tracking;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\social_tracking\Entity\SocialTrackingEntityInterface;

/**
 * Class TrackingBuilderService.
 *
 * @package Drupal\social_tracking
 */
class SocialTrackingBuilderService implements SocialTrackingBuilderServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The route admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $routerAdminContext;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * An array of events.
   *
   * @var array
   */
  protected static $events = [];

  /**
   * PixelBuilderService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The config factory.
   * @param \Drupal\Core\Routing\AdminContext $router_admin_context
   *   The route admin context.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              AccountProxyInterface $current_user,
                              AdminContext $router_admin_context,
                              ModuleHandlerInterface $module_handler,
                              PluginManagerInterface $plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->routerAdminContext = $router_admin_context;
    $this->moduleHandler = $module_handler;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingScriptCode(SocialTrackingEntityInterface $tracking) {

    $script_code = NULL;
    if ($tracking->getTrackingId()) {
      $script_code = str_replace('{{tracking_id}}',
        $tracking->getTrackingId(),
        $this->getSocialTrackingProviderPlugin($tracking)->getCodeScript());

      // Allow other modules to alter the script code.
      $this->moduleHandler->alter('social_tracking_script_code', $script_code, $tracking);
    }

    return $script_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingNoScriptCode(SocialTrackingEntityInterface $tracking) {

    $no_script_code = NULL;
    if ($tracking->getTrackingId()) {
      $no_script_code = str_replace('{{tracking_id}}',
        $tracking->getTrackingId(),
        $this->getSocialTrackingProviderPlugin($tracking)->getCodeNoScript());
      // Allow other modules to alter the noscript code.
      $this->moduleHandler->alter('social_tracking_no_script_code', $no_script_code, $tracking);
    }
    return $no_script_code;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(SocialTrackingEntityInterface $tracking) {
    $tracking_enabled = $tracking->isEnabled();
    $tracking_id = $tracking->getTrackingId();

    if (!$tracking_enabled || !$tracking_id) {
      return FALSE;
    }

    $is_admin_route = $this->routerAdminContext->isAdminRoute();
    $exclude_admin_pages = $tracking->isExcludeAdminPages();
    if ($is_admin_route && $exclude_admin_pages) {
      return FALSE;
    }

    $excluded_roles = array_filter($tracking->getExcludedRoles());
    $current_user_roles = $this->currentUser->getRoles();

    // If the current user has any of excluded roles, then we are returning
    // FALSE.
    foreach ($current_user_roles as $user_role) {
      if (in_array($user_role, $excluded_roles)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSocialTrackingEntities(array $tracking_ids = NULL) {
    return $this->entityTypeManager->getStorage('social_tracking')->loadMultiple($tracking_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getSocialTrackingProviderPlugin(SocialTrackingEntityInterface $tracking) {
    return $this->pluginManager->getTrackingProvider($tracking->getTrackingProvider());
  }

  /**
   * {@inheritdoc}
   */
  public function getSocialTrackingPlugins() {
    $options = [];
    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      $options[$id] = ($definition['label']);
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingScriptLocation(SocialTrackingEntityInterface $tracking) {
    $location = 'head';
    $definition = $this->pluginManager->getTrackingProvider($tracking->getTrackingProvider())->getPluginDefinition();
    if ($definition['scriptLocation']) {
      $location = $definition['scriptLocation'];
    }
    return $location;
  }

}
