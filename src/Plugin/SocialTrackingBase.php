<?php

namespace Drupal\social_tracking\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base class for Social Tracking Type plugins.
 */
abstract class SocialTrackingBase extends PluginBase implements SocialTrackingPluginInterface, SocialTrackingInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCodeNoScript() {
    return "";
  }

}
