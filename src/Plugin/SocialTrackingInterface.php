<?php

namespace Drupal\social_tracking\Plugin;

/**
 * Provides an interface for all Social Tracking Type plugins.
 *
 * The tracking_id in script is a token which will get replaced with actual
 * Tracking ID, while adding script in plugin we need to use {{tracking_id}}
 * token in place of actual tracking id.
 *
 * Usage Example:
 * @code
 * @Tracking(
 *   id = "id",
 *   label = @Translation("label"),
 *   description = @Translation("description"),
 *   script_location = "head"
 * )
 * @endcode
 * 'script_location' can be used to specify where the script needs
 * to added in the head or body.
 */
interface SocialTrackingInterface {

  /**
   * Get the JS script code to be embedded in site.
   *
   * @return string
   *   Return Js Script.
   */
  public function getCodeScript();

  /**
   * Get the No script code to be embedded in site.
   *
   * @return string
   *   Return No JS Script.
   */
  public function getCodeNoScript();

}
