<?php

namespace Drupal\social_tracking\Plugin;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for all Social Tracking plugins.
 */
interface SocialTrackingPluginInterface extends ContainerFactoryPluginInterface, PluginInspectionInterface {

}
