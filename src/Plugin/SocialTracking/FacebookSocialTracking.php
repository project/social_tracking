<?php

namespace Drupal\social_tracking\Plugin\SocialTracking;

use Drupal\social_tracking\Plugin\SocialTrackingBase;

/**
 * Defines a Tracker type for Facebook.
 *
 * @SocialTracking(
 *   id = "facebook",
 *   label = @Translation("Facebook Pixel"),
 *   description = @Translation("A Facebook Pixel Tracking."),
 *   scriptLocation = "head"
 * )
 */
class FacebookSocialTracking extends SocialTrackingBase {

  /**
   * {@inheritdoc}
   *
   * For adding pixel and script guide:
   *
   * @see https://www.facebook.com/business/help/952192354843755?id=1205376682832142
   */
  public function getCodeScript() {
    return "!function(f,b,e,v,n,t,s) {if(f.fbq)return;n=f.fbq=function(){n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)}; if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0'; n.queue=[];t=b.createElement(e);t.async=!0; t.src=v;s=b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s)}(window, document,'script', 'https://connect.facebook.net/en_US/fbevents.js'); fbq('init', '{{tracking_id}}'); fbq('track', 'PageView');";
  }

  /**
   * {@inheritdoc}
   */
  public function getCodeNoScript() {
    return '<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id={{tracking_id}}&ev=PageView&noscript=1"/></noscript>';
  }

}
