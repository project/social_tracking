<?php

namespace Drupal\social_tracking\Plugin\SocialTracking;

use Drupal\social_tracking\Plugin\SocialTrackingBase;

/**
 * Defines a Tracker type for Twitter.
 *
 * @SocialTracking(
 *   id = "twitter",
 *   label = @Translation("Twitter Conversion"),
 *   description = @Translation("A Twitter Conversion Tracking."),
 *   scriptLocation = "body"
 * )
 */
class TwitterSocialTracking extends SocialTrackingBase {

  /**
   * {@inheritdoc}
   *
   * For adding twitter tag and script guide:
   *
   * @see https://developer.twitter.com/en/docs/ads/measurement/overview/conversion-tracking
   */
  public function getCodeScript() {
    return "!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);}, s.version='1.1', s.queue=[], u=t.createElement(n), u.async=!0, u.src='//static.ads-twitter.com/uwt.js', a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script'); twq('init','{{tracking_id}}'); twq('track','PageView');";
  }

}
