<?php

namespace Drupal\social_tracking\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Social Tracking plugin manager.
 */
class SocialTrackingPluginManager extends DefaultPluginManager {

  /**
   * Tracking Provider plugin.
   *
   * @var array
   */
  protected $trackingProvider = [];

  /**
   * Constructor for SocialTrackingPluginManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SocialTracking', $namespaces, $module_handler, 'Drupal\social_tracking\Plugin\SocialTrackingInterface', 'Drupal\social_tracking\Annotation\SocialTracking');
    $this->alterInfo('social_tracking_plugin_info');
    $this->setCacheBackend($cache_backend, 'social_tracking_plugins');
  }

  /**
   * Get Plugin for Tracking Provider.
   *
   * @param string $provider_id
   *   Provider ID.
   *
   * @return mixed
   *   Instance of Tracking plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getTrackingProvider($provider_id) {
    if (!$this->trackingProvider || !isset($this->trackingProvider[$provider_id])) {
      $this->setTrackingProvider($provider_id);
    }
    return $this->trackingProvider[$provider_id];
  }

  /**
   * Set Plugin for Tracking Provider.
   *
   * @param string $provider_id
   *   Provider ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function setTrackingProvider($provider_id) {
    if (!$provider_id) {
      throw new PluginException(sprintf('The provider plugin %s not exists.', $provider_id));
    }
    $this->trackingProvider[$provider_id] = $this->createInstance($provider_id);
  }

}
